import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:charts_flutter/flutter.dart' as charts;

Future<Post> fetchPost() async {
  final response =
  await http.get('https://jsonplaceholder.typicode.com/posts/1');
  final responseJson = json.decode(response.body);

  final testResponse = await http.get('https://api.thingspeak.com/channels/448172/feeds.json?api_key=CYFRSECEG6KJCXOZ&results=5');
  final testResponseJson = json.decode(testResponse.body);

  List<FeedsTemperature> data = new List<FeedsTemperature>();

  for (var feed in testResponseJson['feeds']) {

    DateTime dateTime = DateTime.parse(feed['created_at']);
    int temperature = 0;
    if (feed['field1']!="") {
      temperature = int.parse(feed['field1']);
    }

    data.add(new FeedsTemperature(dateTime, temperature));
  }

  for (int i=0; i<data.length; i++) {
    print(data[i].time);
    print(data[i].temperature);
    print("\n");
  }

  return new Post.fromJson(responseJson);
}

Future<FeedsTemperature> fetchTemp() async {
  final response = await http.get('https://api.thingspeak.com/channels/448172/feeds.json?api_key=CYFRSECEG6KJCXOZ&results=5');
  final responseJson = json.decode(response.body);

  print(responseJson);
  //Map responseJsonArray = json.decode(responseJson);

  //return new FeedsTemperature.fromJson(responseJson)
}

class Post {
  final int userId;
  final int id;
  final String title;
  final String body;

  Post({this.userId, this.id, this.title, this.body});

  factory Post.fromJson(Map<String, dynamic> json) {
    return new Post(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      body: json['body'],
    );
  }
}

class FeedsTemperature {
  final DateTime time;
  final int temperature;

  FeedsTemperature(this.time, this.temperature);
}

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Fetch Data Example',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('Fetch Data Example'),
        ),
        body: new Center(
          child: new FutureBuilder<Post>(
            future: fetchPost(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return new Text(snapshot.data.title);
              } else if (snapshot.hasError) {
                return new Text("${snapshot.error}");
              }

              // By default, show a loading spinner
              return new CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}