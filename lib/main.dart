import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:http/http.dart' as http;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Cabe-cabean',
      theme: new ThemeData(
        primaryColor: Colors.white
      ),
      routes: <String, WidgetBuilder>{
        '/temperature_chart': (BuildContext context) => new SimpleTimeSeriesChart('temperature'),
        '/humidity_chart': (BuildContext context) => new SimpleTimeSeriesChart('humidity'),
        '/moisture_chart': (BuildContext context) => new SimpleTimeSeriesChart('moisture'),
      },
      home: new Home(),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Home'),
      ),
      body: new Center(
        child:
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new Container(
              padding: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 0.0),
              child: new FlatButton(
                  onPressed: () => Navigator.of(context).pushNamed('/temperature_chart'),
                  color: Colors.black87,
                  padding: EdgeInsets.all(12.0),
                  child: Column(
                    children: <Widget>[
                      new Icon(
                        Icons.wb_sunny,
                        color: Colors.white,),
                      new Text(
                        'Suhu Udara',
                        style: new TextStyle(color: Colors.white),
                      )
                    ],
                  ),
              )
            ),
            new Container(
              padding: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 0.0),
                child: new FlatButton(
                  onPressed: () => Navigator.of(context).pushNamed('/humidity_chart'),
                  color: Colors.black87,
                  padding: EdgeInsets.all(12.0),
                  child: Column(
                    children: <Widget>[
                      new Icon(
                        Icons.grain,
                        color: Colors.white,),
                      new Text(
                        'Kelembaban Udara',
                        style: new TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                )
            ),
            new Container(
              padding: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 0.0),
                child: new FlatButton(
                  onPressed: () => Navigator.of(context).pushNamed('/moisture_chart'),
                  color: Colors.black87,
                  padding: EdgeInsets.all(12.0),
                  child: Column(
                    children: <Widget>[
                      new Icon(
                        Icons.landscape,
                        color: Colors.white,),
                      new Text(
                        'Kelembaban Tanah',
                        style: new TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }

}

/*class GoToChartOption extends StatelessWidget {
  final String caption;
  final IconData icon;
  final BuildContext context;

  GoToChartOption(this.caption, this.icon, this.context);

  @override
  Widget build(BuildContext context) {
    return _button();
  }

  Widget _button() {
    return new FlatButton(
      onPressed: () => Navigator.of(context).pushNamed('/chart'),
      color: Colors.black87,
      padding: EdgeInsets.all(12.0),
      child: Column(
        children: <Widget>[
          new Icon(
            icon,
            color: Colors.white,),
          new Text(
            caption,
            style: new TextStyle(color: Colors.white),
          )
        ],
      ),
    );
  }

}*/

class SimpleTimeSeriesChart extends StatelessWidget {
  final bool animate;
  final String caption;

  //SimpleTimeSeriesChart({this.animate});
  SimpleTimeSeriesChart(this.caption, {this.animate});

  @override
  Widget build(BuildContext context) {
    if (caption=='temperature') {
      return _buildTemperature(context);
    } else if (caption=='humidity') {
      return _buildHumidity(context);
    } else {
      return _buildMoisture(context);
    }
  }

  _buildTemperature(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(caption),
        ),
        body: new Center(
          child: new FutureBuilder(
              future: _fetchTemperature(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return new charts.TimeSeriesChart<FeedsTemperature>(
                    _createTemperature(snapshot.data),
                    animate: animate,
                  );
                } else if (snapshot.hasError) {
                  return new Text("${snapshot.error}");
                }

                return new CircularProgressIndicator();
              }),
        )
    );
  }

  _buildHumidity(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(caption),
        ),
        body: new Center(
          child: new FutureBuilder(
              future: _fetchHumidity(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return new charts.TimeSeriesChart<FeedsHumidity>(
                    _createHumidity(snapshot.data),
                    animate: animate,
                  );
                } else if (snapshot.hasError) {
                  return new Text("${snapshot.error}");
                }

                return new CircularProgressIndicator();
              }),
        )
    );
  }

  _buildMoisture(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(caption),
        ),
        body: new Center(
          child: new FutureBuilder(
              future: _fetchMoisture(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return new charts.TimeSeriesChart<FeedsMoisture>(
                    _createMoisture(snapshot.data),
                    animate: animate,
                  );
                } else if (snapshot.hasError) {
                  return new Text("${snapshot.error}");
                }

                return new CircularProgressIndicator();
              }),
        )
    );
  }

}

class FeedsTemperature {
  final DateTime time;
  final int temperature;

  FeedsTemperature(this.time, this.temperature);
}

class FeedsHumidity {
  final DateTime time;
  final int humidity;

  FeedsHumidity(this.time, this.humidity);
}

class FeedsMoisture {
  final DateTime time;
  final int moisture;

  FeedsMoisture(this.time, this.moisture);
}

Future<List<FeedsTemperature>> _fetchTemperature() async {
  final response = await http.get('https://api.thingspeak.com/channels/448172/fields/1.json?api_key=CYFRSECEG6KJCXOZ&results=5');
  final responseJson = json.decode(response.body);

  List<FeedsTemperature> data = new List<FeedsTemperature>();

  for (var feed in responseJson['feeds']) {

    DateTime dateTime = DateTime.parse(feed['created_at']);
    int temperature = 0;
    if (feed['field1']!="") {
      temperature = int.parse(feed['field1']);
    }

    data.add(new FeedsTemperature(dateTime, temperature));
  }

  for (int i=0; i<data.length; i++) {
    print(data[i].time);
    print(data[i].temperature);
    print("\n");
  }

  return data;
}

Future<List<FeedsHumidity>> _fetchHumidity() async {
  final response = await http.get('https://api.thingspeak.com/channels/448172/fields/2.json?api_key=CYFRSECEG6KJCXOZ&results=5');
  final responseJson = json.decode(response.body);

  List<FeedsHumidity> data = new List<FeedsHumidity>();

  for (var feed in responseJson['feeds']) {

    DateTime dateTime = DateTime.parse(feed['created_at']);
    int humidity = 0;
    if (feed['field2']!="") {
      humidity = int.parse(feed['field2']);
    }

    data.add(new FeedsHumidity(dateTime, humidity));
  }

  for (int i=0; i<data.length; i++) {
    print(data[i].time);
    print(data[i].humidity);
    print("\n");
  }

  return data;
}

Future<List<FeedsMoisture>> _fetchMoisture() async {
  final response = await http.get('https://api.thingspeak.com/channels/448172/fields/3.json?api_key=CYFRSECEG6KJCXOZ&results=5');
  final responseJson = json.decode(response.body);

  List<FeedsMoisture> data = new List<FeedsMoisture>();

  for (var feed in responseJson['feeds']) {

    DateTime dateTime = DateTime.parse(feed['created_at']);
    int moisture = 0;
    if (feed['field3']!="") {
      moisture = int.parse(feed['field3']);
    }

    data.add(new FeedsMoisture(dateTime, moisture));
  }

  for (int i=0; i<data.length; i++) {
    print(data[i].time);
    print(data[i].moisture);
    print("\n");
  }

  return data;
}

List<charts.Series<FeedsTemperature, DateTime>> _createTemperature(data) {
  return [
    new charts.Series<FeedsTemperature, DateTime>(
      id: 'Temperature',
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      domainFn: (FeedsTemperature series, _) => series.time,
      measureFn: (FeedsTemperature series, _) => series.temperature,
      data: data,
    )
  ];
}


List<charts.Series<FeedsHumidity, DateTime>> _createHumidity(data) {
  return [
    new charts.Series<FeedsHumidity, DateTime>(
      id: 'Humidity',
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      domainFn: (FeedsHumidity series, _) => series.time,
      measureFn: (FeedsHumidity series, _) => series.humidity,
      data: data,
    )
  ];
}

List<charts.Series<FeedsMoisture, DateTime>> _createMoisture(data) {
  return [
    new charts.Series<FeedsMoisture, DateTime>(
      id: 'Moisture',
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      domainFn: (FeedsMoisture series, _) => series.time,
      measureFn: (FeedsMoisture series, _) => series.moisture,
      data: data,
    )
  ];
}